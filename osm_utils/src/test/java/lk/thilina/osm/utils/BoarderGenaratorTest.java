/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilina.osm.utils;

import lk.thilinaj.osm.parser.OSM;
import lk.thilinaj.osm.parser.OSMLoader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Thilina Jayamini <thilina.k@interblocks.com>
 */
public class BoarderGenaratorTest {
    
    public BoarderGenaratorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createBoaders method, of class BoarderGenarator.
     */
    @Test
    public void testCreateBoaders() throws Exception {
        System.out.println("createBoaders");
        
        OSM osmCoast = OSMLoader.load("C:\\Users\\IB\\Softwares\\map_package\\v2-work\\osm\\coastline.osm");
        OSM bound = OSMLoader.load("C:\\Users\\IB\\Softwares\\map_package\\v2-work\\osm\\Sri_Lanka.boundaries.osm"  );
        
        String outPutFolder = "target\\poly\\";
        BoarderGenarator.createBoaders(bound, osmCoast, outPutFolder);
        
    }
    
}
