/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilina.osm.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lk.thilinaj.osm.parser.Coordinate;
import lk.thilinaj.osm.parser.OSM;
import lk.thilinaj.osm.parser.Relation;
import lk.thilinaj.osm.parser.Way;

/**
 *
 * @author Thilina Jayamini <thilinajayamini@gmail.com>
 */
public class RelationToPoly {

    /*
    public static void main(String[] args) throws Exception {
        RelationToPoly rp = new RelationToPoly();

        OSM osm = OSMLoader.load(args[0]);

        String[] relationIds = args[1].split(",");

        for (String relationId : relationIds) {
            RelationToPoly.writePolyFile(relationId, osm, relationId + ".poly");
        }

        //OSM osm = OSMLoader.load("C:\\Users\\IB\\Softwares\\map_package\\pbf\\Western.osm");
        //rp.writePolyFile("16132", osm, "C:\\Users\\IB\\Softwares\\map_package\\pbf\\Western.poly");
    }*/
    static protected void writePolyFile(String relationId, OSM osm, String outPutFolder, String outPutFile) throws IOException {

        List<String> filterdOnes;
        Relation rel = osm.getRelations().get(relationId);

        filterdOnes = rel.getMembers().stream().filter(t -> t.getType().equals("way")).map(t -> t.getRef()).collect(Collectors.toList());

        mergeAndWrite(osm, filterdOnes, relationId, outPutFolder, outPutFile);

    }

    static protected void mergeAndWrite(OSM osm, List<String> filterdOnes, String relationId, String outPutFolder, String outPutFile) throws IOException {

        Coordinate c;

        List<java.util.List<String>> closedPoly = mergeLine(osm, filterdOnes, relationId);

        closedPoly.sort(new ClosedPolygonSorter());

        int i = 1;

        StringBuilder sb = new StringBuilder();
        sb.append("polygon").append(System.lineSeparator());

        for (java.util.List<String> lst : closedPoly) {

            sb.append(i++).append(System.lineSeparator());

            for (String t : lst) {
                c = osm.getNodes().get(t).getCordinate();
                sb.append("\t").append(String.format("%-10s", c.getLon())).append(" ").append(c.getLat()).append(System.lineSeparator());
            }
            sb.append("END").append(System.lineSeparator());

        }
        sb.append("END").append(System.lineSeparator());

        File folder = new File(outPutFolder);

        if (!folder.exists()) {
            folder.mkdirs();
        }

        System.out.println("Writing " + folder + " : " + outPutFile);
        try (FileWriter fw = new FileWriter(new File(folder, outPutFile))) {
            fw.write(sb.toString());
            fw.flush();
        }
    }

    static protected java.util.List<java.util.List<String>> mergeLine(OSM o, java.util.List<String> line, String elementId) {
        java.util.List<java.util.List<String>> closedPolygons = new java.util.ArrayList<>();
        java.util.List<java.util.List<String>> openWays = new java.util.ArrayList<>();

        line.stream().forEach((s) -> {
            Way w = o.getWays().get(s);
            if (w != null) {
                if (w.isClosed()) {
                    closedPolygons.add(w.getNodes());
                } else {
                    openWays.add(w.getNodes());
                }
            }
        });
        int x = 0;
        int size;

        java.util.List<String> temp = new java.util.ArrayList<>();
        if (!openWays.isEmpty()) {
            temp.addAll(openWays.get(0));
        }

        while (!openWays.isEmpty()) {

            String lastNode = getLast(temp);//get the last node in first node list
            size = temp.size();

            for (int i = 1; i < openWays.size(); i++) {// find the list that contain first node which match with 

                if (getLast(openWays.get(i)).equals(lastNode)) {//now elements are not in same way need to reverse to combine
                    java.util.Collections.reverse(openWays.get(i));
                }
                if (openWays.get(i).get(0).equals(lastNode)) {
                    temp.remove(size - 1);//avoid duplicate node when merging
                    temp.addAll(openWays.get(i));
                    openWays.remove(openWays.get(i));//since it merged no need to keep

                    if (temp.get(0).equals(getLast(temp))) {//after merging check the way is closed.   
                        closedPolygons.add(temp);
                        openWays.remove(0);//remove fist one which is temp
                        if (!openWays.isEmpty()) {
                            temp = new java.util.ArrayList<>();
                            temp.addAll(openWays.get(0));
                        }
                    }
                    break;
                }
            }

            if (!openWays.isEmpty() && size == temp.size()) {//this section cannot ableto find another joining way and its not close. so this is break. need to remove
                System.out.println("Relation  " + elementId + " has unclosed way ");
                openWays.remove(0);
                if (!openWays.isEmpty()) {
                    temp = new java.util.ArrayList<>();
                    temp.addAll(openWays.get(0));
                }
            }

            if (x == 100) {
                System.out.println("there is line breake in relation " + elementId);//TODO improve for partial sea polygons
                break;
            }
        }
        return closedPolygons;
    }

    static protected <T extends Object> T getLast(java.util.List<T> lst) {
        return getByIndex(lst, -1);
    }

    /**
     * get by list index. (-) values for access from last.
     *
     * @param <T>
     * @param lst
     * @param index
     * @return
     */
    static protected <T extends Object> T getByIndex(java.util.List<T> lst, int index) {
        if (index > 0) {
            return lst.get(index);
        }
        return lst.get(lst.size() + index);
    }
}
