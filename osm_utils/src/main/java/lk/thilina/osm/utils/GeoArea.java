/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilina.osm.utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Thilina Jayamini <thilinajayamini@gmail.com>
 */
public class GeoArea {

    double minLat = 90;
    double maxLat = -90;
    double minLon = 180;
    double maxLon = -180;

    List<String> elements = new ArrayList<>();

    /**
     *
     * @param childArea
     * @return
     */
    public boolean isInside(GeoArea childArea) {

        return (this.maxLat > childArea.maxLat && this.minLat < childArea.minLat && this.maxLon > childArea.maxLon && this.minLon < childArea.minLon);

    }

}
