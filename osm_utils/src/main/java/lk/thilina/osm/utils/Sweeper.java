/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilina.osm.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 *
 * @author Thilina Jayamini <thilinajayamini@gmail.com>
 */
public class Sweeper {

    /*
    public static void main(String[] args) throws Exception {

        Sweeper s = new Sweeper();

        s.inOsmFile = "coastline.osm";
        s.outOsmFile = "filted.osm";

        s.sweep();

    }*/

    String inOsmFile = "";
    String outOsmFile = "";

    private void sweep() throws Exception {

        String origLine;
        String tempLine;

        PrintWriter pw;
        try (BufferedReader br = new BufferedReader(new FileReader(inOsmFile))) {
            pw = new PrintWriter(new FileWriter(outOsmFile));
            while (null != (origLine = br.readLine())) {
                
                tempLine = origLine.trim();
                boolean doLoop = false;
                
                if (tempLine.startsWith("<node") || tempLine.startsWith("<way") || tempLine.startsWith("<relation")) {
                    
                    if (tempLine.startsWith("<way") || tempLine.startsWith("<relation")) {
                        doLoop = true;
                    }
                    
                    if (!(tempLine.startsWith("<node") && tempLine.endsWith("/>"))) {
                        doLoop = true;
                    }
                    
                    tempLine = tempLine.split(" ")[2];
                    
                    if (tempLine.startsWith("action")) {
                        
                        tempLine = tempLine.split("=")[1];
                        
                        if (tempLine.charAt(1) == 'd') {
                            
                            System.out.println("deleted line found: " + origLine);
                            
                            while (doLoop) {
                                origLine = br.readLine();
                                System.out.println("deleted line found: " + origLine);
                                if (origLine.trim().startsWith("</")) {
                                    doLoop = false;
                                }
                            }
                            
                        } else {
                            pw.write(origLine);
                            pw.write(System.lineSeparator());
                        }

                    } else {
                        pw.write(origLine);
                        pw.write(System.lineSeparator());
                    }

                } else {
                    pw.write(origLine);
                    pw.write(System.lineSeparator());
                }

            }
        }
        pw.close();

    }

}
