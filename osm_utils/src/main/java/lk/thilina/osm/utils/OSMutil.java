/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilina.osm.utils;

import lk.thilinaj.osm.parser.OSMLoader;

/**
 *
 * @author Thilina Jayamini <thilinajayamini@gmail.com>
 */
public class OSMutil {

    public static void main(String[] args) throws Exception {
        switch (args[0]) {
            case "rp":
                RelationToPoly.writePolyFile(args[1], OSMLoader.load(args[2]), args[3],args[4]);
                break;

            case "br":
                BoarderGenarator.createBoaders(OSMLoader.load(args[1]), OSMLoader.load(args[2]),args[3]);
                break;
        }

    }

}
