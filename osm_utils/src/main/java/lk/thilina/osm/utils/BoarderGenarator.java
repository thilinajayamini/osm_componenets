/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilina.osm.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import lk.thilinaj.osm.parser.Node;
import lk.thilinaj.osm.parser.OSM;
import lk.thilinaj.osm.parser.Relation;
import lk.thilinaj.osm.parser.Way;

/**
 *
 * @author Thilina Jayamini <thilinajayamini@gmail.com>
 */
public class BoarderGenarator {

    private final HashMap<String, StartEndDetai> coastStartEnd = new HashMap<>();
    private final HashMap<String, String> startToCoast = new HashMap<>();
    private final HashMap<String, String> endToCoast = new HashMap<>();
    OSM coastOsm;

    HashMap<String, StartEndDetai> boarderStartEnd;
    HashMap<String, String> startToboarder;
    HashMap<String, String> endToboarder;

    List<GeoArea> islands = new ArrayList<>();

    public static void createBoaders(OSM bound, OSM osmCoast, String outPutFolder) throws Exception {

        BoarderGenarator bg = new BoarderGenarator();
        bg.fill(osmCoast);

        /*
        List<String> temp = new ArrayList<>();
        for (GeoArea land : bg.islands) {
            temp.addAll(land.elements);
        }
        RelationToPoly.mergeAndWrite(bg.coastOsm, temp, "islands", outPutFolder, "islands.poly");
        
         */
        List<Relation> adminBounds = bg.getAdminBoundLst(bound, "2");

        for (Relation adminBound : adminBounds) {

            if (adminBound.getProperty("ISO3166-1").equals("LK")) {

                RelationToPoly.writePolyFile(adminBound.getId(), bound, outPutFolder, adminBound.getProperty("name:en").replace(" ", "_") + ".poly");
                List<String> subareas = adminBound.getMembersByRole("subarea");

                for (String subarea : subareas) {
                    adminBound = bound.getRelations().get(subarea);
                    bg.creteBoader(adminBound.getId(), bound, outPutFolder, adminBound.getProperty("name").replace("Province", "").replace(" ", ""));
                }

            }

        }
        //bg.creteBoader("4630837", bound, "4630837");
        //bg.creteBoader("4630810", bound);
    }

    List<Relation> getAdminBoundLst(OSM bound, String adminLvl) {
        return bound.getRelations().values().stream().filter(r -> r.getTags().get("admin_level").equals(adminLvl)).collect(Collectors.toList());
    }

    private void fill(OSM coast) {
        coastOsm = coast;
        coast.getWays().entrySet().stream().forEach(t -> {
            fillStartEndMap(t.getValue(), coastStartEnd, startToCoast, endToCoast);

        });

        Map<String, StartEndDetai> coastWayMap = new HashMap<>(coastStartEnd);

        List<String> path;
        GeoArea islandDet;
        Map.Entry<String, StartEndDetai> e;

        while (coastWayMap.size() > 0) {
            e = coastWayMap.entrySet().stream().findFirst().get();

            path = getPath(e.getValue().startNdId, e.getValue().startNdId, true);

            if (path.size() > 2 && path.size() < 100) {
                islandDet = new GeoArea();
                islandDet.elements = path;
                calMinMax(islandDet, coastOsm);
                islands.add(islandDet);
            }

            for (String string : path) {
                coastWayMap.remove(string);
            }

        }

    }

    private void fillStartEndMap(Way w, HashMap<String, StartEndDetai> startEnd, HashMap<String, String> startTo, HashMap<String, String> endTo) {
        GeoArea islandDet;

        if (w.getNodes().get(0).equals(RelationToPoly.getLast(w.getNodes()))) {
            //System.out.println("Single line Island found :" + w.getId());
            islandDet = new GeoArea();
            islandDet.elements.add(w.getId());
            calMinMax(islandDet, coastOsm);
            islands.add(islandDet);
        } else {

            StartEndDetai det = new StartEndDetai();

            det.wayId = w.getId();
            det.startNdId = w.getNodes().get(0);
            det.endNdId = RelationToPoly.getLast(w.getNodes());

            startEnd.put(det.wayId, det);
            startTo.put(det.startNdId, det.wayId);
            endTo.put(det.endNdId, det.wayId);
        }
    }

    private void creteBoader(String relId, OSM bound, String outPutFolder, String outputFileName) throws IOException {

        System.out.println("Creating " + outputFileName);

        Relation rel = bound.getRelations().get(relId);
        List<String> membersByRole = rel.getMembers().stream().filter(t -> t.getType().equals("way")).map(t -> t.getRef()).collect(Collectors.toList());

        boarderStartEnd = new HashMap<>();
        startToboarder = new HashMap<>();
        endToboarder = new HashMap<>();

        membersByRole.stream().forEach(t -> {

            fillStartEndMap(bound.getWays().get(t), boarderStartEnd, startToboarder, endToboarder);

        });

        List<String> shoreLine = getShoreLine(boarderStartEnd);
        RelationToPoly.writePolyFile(relId, bound, outPutFolder, outputFileName + ".poly");

        if (shoreLine != null && !shoreLine.isEmpty()) {

            writePoly(shoreLine, bound, outPutFolder, outputFileName);
        }
        System.out.println("");

    }

    private void writePoly(List<String> shoreLine, OSM bound, String outPutFolder, String outputFileName) throws IOException {

        OSM merged = new OSM();
        merged.getNodes().putAll(coastOsm.getNodes());
        merged.getNodes().putAll(bound.getNodes());
        merged.getWays().putAll(coastOsm.getWays());
        merged.getWays().putAll(bound.getWays());

        String borderStartNd = coastStartEnd.get(shoreLine.get(0)).startNdId;
        String borderEndNd = coastStartEnd.get(RelationToPoly.getLast(shoreLine)).endNdId;

        List<String> boardPath = new ArrayList<>();
        List<String> boardPath2 = new ArrayList<>(boarderStartEnd.keySet());

        List<String> islands = getIslands(boardPath2, merged);

        String tmpStartNd = borderStartNd;
        boolean isBoarderPathIsCountryBoader = false;

        while (!tmpStartNd.equals(borderEndNd)) {
            tmpStartNd = getNextNd(tmpStartNd, boardPath, boardPath2);
        }

        for (String boarder : boardPath) {
            if (bound.getWays().get(boarder).getTags().get("admin_level").equals("2")) {
                isBoarderPathIsCountryBoader = true;
                break;
            }
        }

        shoreLine.addAll(islands);
        boardPath.addAll(shoreLine);
        boardPath2.addAll(shoreLine);

        if (isBoarderPathIsCountryBoader) {

            RelationToPoly.mergeAndWrite(merged, boardPath, outputFileName, outPutFolder, outputFileName + "-sea.poly");
            RelationToPoly.mergeAndWrite(merged, boardPath2, outputFileName, outPutFolder, outputFileName + "-land.poly");
        } else {

            RelationToPoly.mergeAndWrite(merged, boardPath2, outputFileName, outPutFolder, outputFileName + "-sea.poly");
            RelationToPoly.mergeAndWrite(merged, boardPath, outputFileName, outPutFolder, outputFileName + "-land.poly");
        }

    }

    /**
     * split boundary path to 2 sections
     *
     * @param borderStartNd
     * @param boardPath
     * @param boardPath2
     * @return
     */
    private String getNextNd(String borderStartNd, List<String> boardPath, List<String> boardPath2) {
        for (StartEndDetai det : boarderStartEnd.values()) {

            if (det.startNdId.equals(borderStartNd) && boardPath2.contains(det.wayId)) {
                boardPath2.remove(det.wayId);
                boardPath.add(det.wayId);
                return det.endNdId;
            } else if (det.endNdId.equals(borderStartNd) && boardPath2.contains(det.wayId)) {
                boardPath.add(det.wayId);
                boardPath2.remove(det.wayId);
                return det.startNdId;
            }

        }
        return null;
    }

    private List<String> getShoreLine(HashMap<String, StartEndDetai> boarderStartEnd) {
        String coastStartNd = null;
        String coastEndNd = null;

        for (Map.Entry<String, StartEndDetai> e : boarderStartEnd.entrySet()) {
            if (coastStartNd == null && startToCoast.containsKey(e.getValue().startNdId) && !(coastEndNd != null && coastEndNd.equals(e.getValue().startNdId))) {
                coastStartNd = e.getValue().startNdId;
                System.out.println(e.getKey() + " border element start form start cost line :" + e.getValue().startNdId);
            } else if (coastEndNd == null && endToCoast.containsKey(e.getValue().startNdId) && !(coastStartNd != null && coastStartNd.equals(e.getValue().startNdId))) {
                coastEndNd = e.getValue().startNdId;
                System.out.println(e.getKey() + " border element start form end cost line :" + e.getValue().startNdId);
            } else if (coastStartNd == null && startToCoast.containsKey(e.getValue().endNdId) && !(coastEndNd != null && coastEndNd.equals(e.getValue().endNdId))) {
                coastStartNd = e.getValue().endNdId;
                System.out.println(e.getKey() + " border element end form start cost line :" + e.getValue().endNdId);
            } else if (coastEndNd == null && endToCoast.containsKey(e.getValue().endNdId) && !(coastStartNd != null && coastStartNd.equals(e.getValue().endNdId))) {
                coastEndNd = e.getValue().endNdId;
                System.out.println(e.getKey() + " border element end form end cost line :" + e.getValue().endNdId);
            }

            if (coastStartNd != null && coastEndNd != null) {
                break;
            }

        }

        if (coastStartNd == null || coastEndNd == null) {
            System.out.println("No coast line for this bound!!!!");
            return null;
        }

        System.out.println("coast start : " + coastStartNd + " end : " + coastEndNd);

        List<String> parthOne = getPath(coastStartNd, coastEndNd, false);
        List<String> parthTwo = getPath(coastEndNd, coastStartNd, false);

        if (getPathLength(parthOne) > getPathLength(parthTwo)) {
            return parthTwo;
        } else {
            return parthOne;
        }
    }

    private List<String> getPath(String startNd, String endNd, boolean skipFirst) {

        List<String> path = new ArrayList<>();
        StartEndDetai det;
        String localStart = startNd;

        while (!localStart.equals(endNd) || skipFirst) {

            skipFirst = false;
            det = coastStartEnd.get(startToCoast.get(localStart));
            if (det == null) {
                return path;
            }
            path.add(det.wayId);
            localStart = det.endNdId;
        }
        return path;
    }

    private int getPathLength(List<String> path) {
        int lgth = 0;

        lgth = path.stream().map((string) -> coastOsm.getWays().get(string).getNodes().size()).reduce(lgth, Integer::sum);
        return lgth;
    }

    void calMinMax(GeoArea det, OSM osm) {

        double[][] bound;

        for (String e : det.elements) {

            Way w = osm.getWays().get(e);
            if (w == null || w.getNodes() == null || w.getNodes().isEmpty()) {
                System.out.println("troubledata for :" + e);
                continue;
            }

            bound = new double[w.getNodes().size() + 1][];
            int i = -1;

            for (String ns : w.getNodes()) {
                Node n = osm.getNodes().get(ns);

                bound[++i] = n.getCordinate().getPossition();

                if (det.maxLat < bound[i][0]) {
                    det.maxLat = bound[i][0];
                }
                if (det.minLat > bound[i][0]) {
                    det.minLat = bound[i][0];
                }

                if (det.maxLon < bound[i][1]) {
                    det.maxLon = bound[i][1];
                }
                if (det.minLon > bound[i][1]) {
                    det.minLon = bound[i][1];
                }

            }

        }

    }

    private List<String> getIslands(List<String> boardPath2, OSM osm) {

        GeoArea ga = new GeoArea();
        ga.elements = boardPath2;
        calMinMax(ga, osm);
        List<String> islandParts = new ArrayList<>();
        List<GeoArea> remainingIslands = new ArrayList<>();

        islands.forEach((GeoArea island) -> {
            if (ga.isInside(island)) {

                islandParts.addAll(island.elements);

            } else {
                remainingIslands.add(island);
            }
        });
        islands = remainingIslands;
        return islandParts;

    }

}
