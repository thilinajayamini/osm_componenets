/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilina.osm.utils;

import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Thilina Jayamini <thilina.k@interblocks.com>
 */
public class ClosedPolygonSorter implements Comparator<List<String>> {

    @Override
    public int compare(List<String> o1, List<String> o2) {
        return o2.size() - o1.size();
    }

}
