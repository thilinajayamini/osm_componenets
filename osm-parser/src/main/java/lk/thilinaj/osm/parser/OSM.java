/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.parser;

import java.util.HashMap;

/**
 *
 * @author Thilina Jayamini
 */
public class OSM {

    java.util.HashMap<String, Node> nodes;
    java.util.HashMap<String, Way> ways;
    java.util.HashMap<String, Relation> relations;

    //minlat, minlon, maxlat, maxlon;    
    String[] bounds = new String[4];

    public OSM() {
        this.nodes = new HashMap<>();
        this.ways = new HashMap<>();
        this.relations = new HashMap<>();
    }

    public HashMap<String, Node> getNodes() {
        return nodes;
    }

    public HashMap<String, Way> getWays() {
        return ways;
    }

    public HashMap<String, Relation> getRelations() {
        return relations;
    }

    public void clearNodes() {
        this.nodes = null;
    }

    public void clearWays() {
        this.ways = null;
    }

    public void clearRelations() {
        this.relations = null;
    }
    
    public void clear(){
        clearNodes();
        clearWays();
        clearRelations();        
    }

    public java.util.ArrayList<Coordinate> getBounds() {
        java.util.ArrayList<Coordinate> c = new java.util.ArrayList<>();
        c.add(new Coordinate(bounds[0], bounds[1]));
        c.add(new Coordinate(bounds[2], bounds[1]));
        c.add(new Coordinate(bounds[2], bounds[3]));
        c.add(new Coordinate(bounds[0], bounds[3]));
        return c;
    }

}
