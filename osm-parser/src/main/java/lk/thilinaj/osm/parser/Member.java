/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.parser;

/**
 *
 * @author Thilina Jayamini
 *
 */
public class Member {

    String type;
    String ref;
    String role;

    public Member(String type, String ref, String role) {
        this.type = type;
        this.ref = ref;
        this.role = role;
    }

    public Member() {
    }

    public String getType() {
        return type;
    }

    public String getRef() {
        return ref;
    }

    public String getRole() {
        return role;
    }

}
