/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.parser;

import java.util.Objects;

/**
 *
 * @author Thilina Jayamini
 */
public class Coordinate {

    String lat;
    String lon;
    String geoHash;
    int hashA = 0;

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    public Coordinate(String lat, String lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public Coordinate(double lat, double lon) {
        this.lat = String.valueOf(lat);
        this.lon = String.valueOf(lon);
    }

    public Coordinate(double[] location) {
        this.lat = String.valueOf(location[0]);
        this.lon = String.valueOf(location[1]);
    }

    public Coordinate() {
    }

    @Override
    public String toString() {
        return "(" + lat + "," + lon + ") ";
    }

    public double[] getPossition() {
        return new double[]{Double.parseDouble(this.lat), Double.parseDouble(this.lon)};
    }

    @Override
    public int hashCode() {
        if (this.hashA == 0) {
            this.hashA = calculateHash();
        }
        return hashA;
    }

    private int calculateHash() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.lat);
        hash = 17 * hash + Objects.hashCode(this.lon);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Coordinate other = (Coordinate) obj;
        if (!this.lat.equals(other.lat)) {
            return false;
        }
        return this.lon.equals(other.lon);
    }

    public String getGeoHash() {
        char[] latArr = this.lat.toCharArray();
        char[] lonArr = this.lon.toCharArray();

        char[] com = new char[latArr.length + lonArr.length];
        int j = 0;
        for (int i = 0; i < com.length - 1; i += 2) {
            if (j < latArr.length) {
                com[i] = latArr[j];
            } else {
                com[i] = '0';
            }
            if (j < lonArr.length) {
                com[++i] = lonArr[j];
            } else {
                com[i] = '0';
            }
            j++;
        }
        return new String(com);
    }

}
