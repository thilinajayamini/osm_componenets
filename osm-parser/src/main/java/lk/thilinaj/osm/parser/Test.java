/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.parser;

/**
 *
 * @author Thilina Jayamini <thilinajayamini@gmail.com>
 */
class Test {

    public static void main(String[] args) throws Exception {
        long start;
        //String fileName = "mataraWithBouds.osm";
        //String fileName = "Sri_Lanka.osm";
        String fileName = "my.osm";

        for (int i = 0; i < 4; i++) {
            start = System.currentTimeMillis();
            StaXDataLoader.loadData(fileName);
            System.out.println("Total New : " + (System.currentTimeMillis() - start));

            Thread.sleep(10000);

            start = System.currentTimeMillis();
            LegacyDataLoader.loadData(fileName);
            System.out.println("Total Old : " + (System.currentTimeMillis() - start));
            Thread.sleep(10000);
        }

    }
}
