/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.parser;

/**
 *
 * @author Thilina Jayamini
 */
public class Node extends Element {

    Coordinate c;

    public Node() {
        this.c = new Coordinate();
    }

    public Node(Coordinate c) {
        this.c = c;
    }

    public Node(String id, String lat, String lon) {
        super(id);        
        /*try{
            Long.parseLong(id);
            Double.parseDouble(lat);
            Double.parseDouble(lon);
        }catch(Exception e){
            System.out.println("id :"+ id +" lat:" + lat+" lon :"+lon);
        }*/
        this.c = new Coordinate(lat, lon);
    }

    public Coordinate getCordinate() {
        return c;
    }

}
