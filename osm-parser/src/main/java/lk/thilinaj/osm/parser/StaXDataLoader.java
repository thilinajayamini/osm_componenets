/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.parser;

import java.io.FileReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author Thilina Jayamini <thilinajayamini@gmail.com>
 */
public class StaXDataLoader {

    private static int iLat, iLon;

    public static OSM loadData(String fileName) throws Exception {

        detectIndex(fileName);

        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader sr = factory.createXMLStreamReader(new FileReader(fileName));
        OSM osm = new OSM();
        Element e = new Element();
        while (sr.hasNext()) {

            switch (sr.next()) {
                case XMLEvent.START_ELEMENT://TODO - bug in this parser

                    switch (sr.getLocalName()) {
                        case "tag":
                            addTag(e, sr);
                            break;
                        case "nd":
                            ((Way) e).nodes.add(getAv(sr, 0));
                            break;
                        case "node":
                            e = creteNode(sr);
                            osm.nodes.put(e.getId(), (Node) e);
                            break;
                        case "way":
                            e = creteWay(sr);
                            osm.ways.put(e.getId(), (Way) e);
                            break;
                        case "member":
                            ((Relation) e).members.add(creteMember(sr));
                            break;
                        case "relation":
                            e = creteRelation(sr);
                            osm.relations.put(e.getId(), (Relation) e);
                            break;
                        case "bounds":
                            osm.bounds = getBounds(sr);
                            break;
                    }

                    break;

            }

        }
        return osm;

    }

    public static String getAv(XMLStreamReader r, int index) {

        return r.getAttributeValue(index);
    }

    public static Node creteNode(XMLStreamReader sr) {

        return new Node(getAv(sr, 0), getAv(sr, iLat), getAv(sr, iLon));
    }

    public static Way creteWay(XMLStreamReader sr) {
        return new Way(getAv(sr, 0));
    }

    public static Relation creteRelation(XMLStreamReader sr) {
        return new Relation(getAv(sr, 0));
    }

    public static Member creteMember(XMLStreamReader sr) {
        return new Member(getAv(sr, 0), getAv(sr, 1), getAv(sr, 2));
    }

    public static void addTag(Element e, XMLStreamReader sr) {
        e.addTag(getAv(sr, 0), getAv(sr, 1));
    }

    public static String[] getBounds(XMLStreamReader sr) {

        return new String[]{getAv(sr, 0), getAv(sr, 1), getAv(sr, 2), getAv(sr, 3)};
    }

    public static void detectIndex(String fileName) throws Exception {
        try (java.io.BufferedReader br = new java.io.BufferedReader(new FileReader(fileName))) {
            String s;
            while (true) {
                s = br.readLine();
                if (s.trim().startsWith("<node")) {
                    String[] arr = s.trim().split(" ");

                    for (int i = 0; i < arr.length; i++) {
                        if (arr[i].startsWith("lat")) {
                            iLat = i - 1;
                        } else if (arr[i].startsWith("lon")) {
                            iLon = i - 1;
                        }
                    }
                    break;
                }
            }
        }

    }
}
