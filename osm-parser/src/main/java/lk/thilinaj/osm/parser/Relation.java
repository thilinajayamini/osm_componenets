/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.parser;

import java.util.stream.Collectors;

/**
 *
 * @author Thilina Jayamini
 */
public class Relation extends Element {

    java.util.ArrayList<Member> members;

    public Relation() {
        this.members = new java.util.ArrayList<>();
    }

    public Relation(String id) {
        super(id);
        this.members = new java.util.ArrayList<>();
    }

    public String getType() {
        return getProperty("type");
    }

    public String getProperty(String key) {

        return this.getTags().getOrDefault(key, "");

    }

    public java.util.List<Member> getMembers() {
        return this.members;
    }

    public java.util.List<String> getMembersByRole(String role) {
        return this.getMembers().stream().filter((m) -> m.getRole().equals(role)).map((m) -> m.getRef()).collect(Collectors.toList());
    }

    /**
     *
     * @param type Type of the member eg node,way, relation.
     * @param role Role of the member eg: inner , outer.
     * @return true if present .
     */
    public boolean isMembersPresentWithType(String type, String role) {
        return this.getMembers().stream().anyMatch((m) -> m.getRole().equals(role) && m.getType().equals(type));
    }

    public boolean isValidRelation(OSM o) {
        if (this.getType() == null) {
            System.err.println("No type for relation id:" + this.getId());
            return false;
        }
        boolean result = true;
        for (Member m : members) {
            switch (m.type) {
                case "node":
                    result = o.getNodes().get(m.ref) != null;
                    break;
                case "way":
                    result = o.getWays().get(m.ref) != null;
                    break;
                case "relation":
                    result = o.getRelations().get(m.ref) != null;
                    break;
            }
            if (!result) {
                return result;
            }
        }
        return result;
    }

}
