/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.parser;

/**
 *
 * @author Thilina Jayamini
 */
public class Element {

    private String id;
    private java.util.Map<String, String> tags;
    //java.util.ArrayList<String> tags;

    public Element() {
        this.tags = new java.util.TreeMap<>();
    }

    public Element(String id) {
        this();
        this.id = id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    //public HashMap<String, String> getTags() {
    //    return tags;
    //}
    public java.util.Map<String, String> getTags() {
        return tags;
    }

    public String addTag(String k, String v) {
        return this.tags.put(k, v);
    }
}
