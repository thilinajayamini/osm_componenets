/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.parser;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 *
 * @author Thilina Jayamini
 */
class LegacyDataLoader {

    static String[] parts;

    public static OSM loadData(String fileName) throws Exception {

        FileReader fr = new FileReader(fileName);
        OSM osm;
        try (BufferedReader br = new BufferedReader(fr)) {
            String line;
            osm = new OSM();
            Element e = new Element();
            String key;
            while (null != (line = br.readLine())) {
                line = line.trim();
                if (line == null || line.length() < 5) {
                    continue;
                }
                key = line.substring(1, 4).trim();

                switch (key) {//TODO optimazation
                    case "nod":
                        e = creteNode(line);
                        osm.nodes.put(e.getId(), (Node) e);
                        break;
                    case "way":
                        e = creteWay(line);
                        break;
                    case "/wa":
                        osm.ways.put(e.getId(), (Way) e);
                        break;
                    case "rel":
                        e = creteRelation(line);
                        break;
                    case "/re":
                        osm.relations.put(e.getId(), (Relation) e);
                        break;
                    case "nd":
                        ((Way) e).nodes.add(parseNd(line));
                        break;
                    case "mem":
                        ((Relation) e).members.add(creteMember(line));
                        break;
                    case "tag":
                        addTag(e, line);
                        break;
                    case "bou":
                        osm.bounds = getBounds(line);
                        break;
                }
            }
        }
        return osm;
    }

    private static Node creteNode(String line) {
        Node n = new Node();
        parts = line.split("\"");

        n.setId(parts[1]);

        for (int i = 2; i < parts.length; i++) {
            switch (parts[i]) {
                case " lat=":
                    i++;
                    n.c.lat = parts[i];
                    break;
                case " lon=":
                    i++;
                    n.c.lon = parts[i];
                    i = parts.length;
                    break;
            }
        }

        return n;
    }

    private static Way creteWay(String line) {
        Way w = new Way();
        return (Way) addIdToElement(w, line);
    }

    private static Relation creteRelation(String line) {
        Relation r = new Relation();
        return (Relation) addIdToElement(r, line);
    }

    private static String parseNd(String line) {
        return extractValue(line, 1);
    }

    private static Member creteMember(String line) {
        Member m = new Member();
        parts = line.split("\"");

        for (int i = 0; i < parts.length; i++) {
            switch (parts[i]) {
                case "<member type=":
                    i++;
                    m.type = parts[i];
                    break;
                case " ref=":
                    i++;
                    m.ref = parts[i];
                    break;
                case " role=":
                    i++;
                    m.role = parts[i];
                    break;
            }
        }

        return m;
    }

    private static void addTag(Element e, String line) {
        parts = line.split("\"");

        if (parts.length < 4) {
            System.out.println(line);
            return;
        }
        e.addTag(parts[1], parts[3]);

    }

    private static Element addIdToElement(Element e, String line) {
        e.setId(extractValue(line, 1));
        return e;
    }

    private static String extractValue(String line, int index) {
        parts = line.split("\"");
        return parts[index];

    }

    private static String[] getBounds(String line) {
        parts = line.split("\"");
        return new String[]{parts[1], parts[3], parts[5], parts[7]};
    }
}
