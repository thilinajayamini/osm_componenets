/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.parser;

/**
 *
 * @author Thilina Jayamini
 */
public class Way extends Element {

    java.util.ArrayList<String> nodes;

    public Way() {
        this.nodes = new java.util.ArrayList<>();
    }

    public Way(String id) {
        this();
        this.setId(id);
    }

    public java.util.List<String> getNodes() {
        return this.nodes;
    }

    public boolean isClosed() {
        return this.nodes.get(0).equals(this.nodes.get(this.length() - 1));
    }

    public int length() {
        return this.nodes.size();
    }

    /**
     * this will split way in to given size
     *
     * @param size size of the parts that need to split this way.
     * @return
     */
    public java.util.List<Way> splitToSize(int size) {

        int lenth = this.nodes.size();
        java.util.List<Way> lst = new java.util.ArrayList<>();
        if (lenth <= size) {
            lst.add(this);
            return lst;
        }
        size -= 1;
        Way w;
        int i = 0;
        int from, to;
        boolean condition = true;
        while (condition) {
            w = new Way();
            w.setId(this.getId() + ":" + i);
            w.getTags().putAll(this.getTags());
            from = i * size;
            i++;
            to = i * size;
            if (size < lenth) {
                w.nodes.addAll(this.nodes.subList(from, to + 1));
            } else {
                w.nodes.addAll(this.nodes.subList(from, this.nodes.size()));
                condition = false;
            }
            lst.add(w);

            lenth -= size;
        }
        return lst;

    }

    /**
     * this will split way in given index
     *
     * @param index position where need to split the way
     * @return list ow 2 elements
     */
    public java.util.List<Way> splitHere(int index) {
        java.util.List<Way> lst = new java.util.ArrayList<>(2);
        lst.add(this.subWay(0, index, 0));
        lst.add(this.subWay(index - 1, this.length(), 1));
        return lst;
    }

    /**
     * Returns a view of the portion of this list between the specified
     * fromIndex, inclusive, and toIndex, exclusive. (If fromIndex and toIndex
     * are equal, the returned list is empty.) .
     *
     * @param fromInex start
     * @param toIndex end
     * @return part of this way
     */
    public Way subWay(int fromInex, int toIndex) {
        return subWay(fromInex, toIndex, 1);
    }

    /**
     * Returns a view of the portion of this list between the specified
     * fromIndex, inclusive, and toIndex, exclusive. (If fromIndex and toIndex
     * are equal, the returned list is empty.) .
     *
     * @param fromInex start
     * @param toIndex end
     * @param subWayId sub id of newly created way (new way id = original way
     * id:subway id)
     * @return part of this way
     */
    public Way subWay(int fromInex, int toIndex, int subWayId) {
        Way w;
        w = new Way();
        w.setId(this.getId() + ":" + subWayId);
        w.getTags().putAll(this.getTags());
        w.nodes.addAll(this.nodes.subList(fromInex, toIndex));
        return w;
    }

}
