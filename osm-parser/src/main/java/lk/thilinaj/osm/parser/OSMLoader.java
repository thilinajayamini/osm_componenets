/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.parser;

/**
 *
 * @author Thilina Jayamini <thilinajayamini@gmail.com>
 */
public class OSMLoader {
    
    public static OSM loadLegacy(String fileName) throws Exception {
        validateFile(fileName);
        return LegacyDataLoader.loadData(fileName);
    }
    
    public static OSM load(String fileName) throws Exception {
        validateFile(fileName);
        return StaXDataLoader.loadData(fileName);
    }
    
    private static void validateFile(String fileName) {
        java.io.File f = new java.io.File(fileName);
        if (!f.exists()) {
            System.err.println("Input File not exists.File :" + fileName);
            System.exit(1);
        }
    }
}
