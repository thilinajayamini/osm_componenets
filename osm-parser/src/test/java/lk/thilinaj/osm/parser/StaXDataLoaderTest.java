/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.thilinaj.osm.parser;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Thilina Jayamini - thilinajayamini@gmail.com
 */
public class StaXDataLoaderTest {

    static OSM o;

    public StaXDataLoaderTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        String fileName = "Mangaia.osm";

        try {
            o = StaXDataLoader.loadData(fileName);
        } catch (Exception ex) {
            System.out.println(ex.getCause().toString());
        }
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of loadData method, of class StaXDataLoader.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testLoadData() throws Exception {
        System.out.println("loadData");
        assertNotNull(o);
    }

    /**
     * Test of creteNode method, of class StaXDataLoader.
     */
    @Test
    public void testCreteNode() {
        System.out.println("creteNode");
        /*<node id="240542446" visible="true" version="2" changeset="34951861" timestamp="2015-10-29T15:31:58Z" user="Thilina" uid="746498" lat="-21.9012498" lon="-157.9482978"/>
         <node id="2404061643" lat="6.2782087" lon="81.0546721" version="1" timestamp="2013-08-01T19:37:36Z" changeset="17183878" uid="35273" user="Ernst Poulsen"/>
         <node id="1763565594" visible="true" version="2" changeset="34952067" timestamp="2015-10-29T15:40:04Z" user="Thilina" uid="746498" lat="-21.9194053" lon="-157.8895458">
         <tag k="name" v="Ara Moana Bungalows"/>
         <tag k="tourism" v="hotel"/>
         </node>
         */
        Node result = o.getNodes().get("240542446");
        assertEquals("-21.9012498", result.getCordinate().getLat());

        result = o.getNodes().get("1763565594");
        assertEquals(2, result.getTags().size());
        assertEquals("hotel", result.getTags().get("tourism"));
    }

    /**
     * Test of creteWay method, of class StaXDataLoader.
     */
    @Test
    public void testCreteWay() {
        System.out.println("creteWay");
        /*<way id="377479022" visible="true" version="1" changeset="34951861">
         <nd ref="254387267" />
         <nd ref="254387268" />
         <nd ref="254387269" />
         <nd ref="240542722" />
         <nd ref="240542721" />
         <nd ref="240542720" />
         <nd ref="240542719" />
         <tag k="name" v="Mangaia" />
         <tag k="natural" v="coastline" />
         <tag k="place" v="island" />
         <tag k="source" v="PGS" />
         <tag k="wikipedia" v="en:Mangaia" />
         </way>*/
        Way result = o.getWays().get("377479022");
        assertEquals(7, result.getNodes().size());
        assertEquals("254387267", result.getNodes().get(0));
        assertEquals(5, result.getTags().size());
        assertEquals("coastline", result.getTags().get("natural"));

    }

    /**
     * Test of creteRelation method, of class StaXDataLoader.
     */
    @Test
    public void testCreteRelation() {
        System.out.println("creteRelation");
        /*
         <relation id="5623584" visible="true" version="1" changeset="34951861">
         <member type="way" ref="377479019" role="outer" />
         <member type="way" ref="377479015" role="outer" />
         <member type="way" ref="377479023" role="outer" />
         <tag k="natural" v="grassland" />
         <tag k="type" v="multipolygon" />
         </relation>
         */

        Relation result = o.getRelations().get("5623584");
        assertEquals(3, result.getMembers().size());
        assertEquals("377479019", result.getMembers().get(0).ref);
        assertEquals("way", result.getMembers().get(0).type);
        assertEquals("outer", result.getMembers().get(0).role);
        assertEquals(2, result.getTags().size());
        assertEquals("multipolygon", result.getTags().get("type"));

    }

    /**
     * Test of getBounds method, of class StaXDataLoader.
     */
    @Test
    public void testGetBounds() {
        System.out.println("getBounds");
        //<bounds minlat="-22.1580699" minlon="-158.1676079" maxlat="-21.6903438" maxlon="-157.6677406" origin="CGImap 0.4.0 (15357 thorn-01.openstreetmap.org)" />
        java.util.List<Coordinate> result = o.getBounds();
        assertEquals(4, result.size());
        assertEquals("-21.9774000", result.get(0).lat);
        assertEquals("-157.9916000", result.get(0).lon);

    }

}
